#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <conio.h>
#include <math.h>
/*
 * MDPGen.c // Open Source Password Generator in C
 *
 *  Cr: 20 avr. 2020
 *
 *      Auteur: StayNoided
 *
 * 26,52,62,70
 */



int genChiffre(int _iMin, int _iMax){
        /* Fait un chiffre au hasard */
	    return (_iMin + (rand () % (_iMax-_iMin+1)));
}
int charAff(int Valeur){
    /* Ceci affecte une valeur*/
	int Cartons;
	 switch(Valeur)
	    {
	        case 1 :
	        	printf("a");
	        break;
	        case 2:
	        	printf("b");
	        break;
	        case 3:
	        	printf("c");
	        break;
	        case 4 :
	        	printf("d");
	        break;
	        case 5 :
	        	printf("e");
	        break;
	        case 6 :
	            printf("f");
	        break;
	        case 7 :
	            printf("g");
	        break;
	        case 8 :
	            printf("h");
	        break;
	        case 9 :
	            printf("i");
	        break;
	        case 10 :
                printf("j");
	        break;
	        case 11 :
	        	printf("k");
	        break;
	        case 12 :
	            printf("l");
	        break;
	        case 13 :
	            printf("m");
	        break;
	        case 14 :
	            printf("n");
	        break;
	        case 15 :
	            printf("o");
	        break;
	        case 16 :
	        	printf("p");
	        break;
            case 17 :
	            printf("q");
	        break;
	        case 18 :
	            printf("r");
	        break;
	        case 19 :
	            printf("s");
	        break;
	        case 20 :
	        	printf("t");
	        break;
            case 21 :
	        	printf("u");
	        break;
	        case 22:
	        	printf("v");
	        break;
	        case 23:
	        	printf("w");
	        break;
	        case 24 :
	        	printf("x");
	        break;
	        case 25 :
	        	printf("y");
	        break;
	        case 26 :
	            printf("z");
	        break;
            case 27 :
	        	printf("A");
	        break;
	        case 28:
	        	printf("B");
	        break;
	        case 29:
	        	printf("C");
	        break;
	        case 30 :
	        	printf("D");
	        break;
	        case 31 :
	        	printf("E");
	        break;
	        case 32 :
	            printf("F");
	        break;
	        case 33 :
	            printf("G");
	        break;
	        case 34 :
	            printf("H");
	        break;
	        case 35 :
	            printf("I");
	        break;
	        case 36 :
                printf("J");
	        break;
	        case 37 :
	        	printf("K");
	        break;
	        case 38 :
	            printf("L");
	        break;
	        case 39 :
	            printf("M");
	        break;
	        case 40 :
	            printf("N");
	        break;
	        case 41 :
	            printf("O");
	        break;
	        case 42 :
	        	printf("P");
	        break;
            case 43 :
	            printf("Q");
	        break;
	        case 44 :
	            printf("R");
	        break;
	        case 45 :
	            printf("S");
	        break;
	        case 46 :
	        	printf("T");
	        break;
            case 47 :
	        	printf("U");
	        break;
	        case 48:
	        	printf("V");
	        break;
	        case 49:
	        	printf("W");
	        break;
	        case 50 :
	        	printf("X");
	        break;
	        case 51 :
	        	printf("Y");
	        break;
	        case 52 :
	            printf("Z");
	        break;
            case 53 :
	        	printf("0");
	        break;
	        case 54:
	        	printf("1");
	        break;
	        case 55:
	        	printf("2");
	        break;
	        case 56 :
	        	printf("3");
	        break;
	        case 57 :
	        	printf("4");
	        break;
	        case 58 :
	            printf("5");
	        break;
	        case 59 :
	            printf("6");
	        break;
	        case 60 :
	            printf("7");
	        break;
	        case 61 :
	            printf("8");
	        break;
	        case 62 :
                printf("9");
	        break;
	        case 63 :
	        	printf("-");
	        break;
	        case 64 :
	            printf("_");
            break;
            case 65 :
	        	printf("$");
	        break;
	        case 66 :
	            printf("@");
            break;
            case 67 :
	        	printf("!");
	        break;
	        case 68 :
	            printf("?");
            break;
            case 69 :
	        	printf(".");
	        break;
	        case 70 :
	            printf("%");
            break;
            case 71 :
                printf("");
            break;
            case 72 :
	        	printf("&");
	        break;
	        case 73 :
	            printf("#");
            break;
            case 74 :
                printf("/");
            break;
            default :
                printf("0");
            break;
	    }
	 return Cartons;
	 /* Ceci attribue la valeur
	  *
	  */
}
void sleep(unsigned long int n) {
        /* boucle vide parcourue (n * 100000) fois*/
        int i = 0;
        unsigned long int max = n * 100000;
        do {
                /* Faire qqch de stupide qui prend du temps */
                i++;
        }
        while(i <= max);
}


void main() {
    int waitCycle=0, mdpLength=0, nbCharMax, nbChar=0, i=1,x, pu=1;
    printf("Welcome to:\n");
    printf("o     o ooo.    .oPYo. .oPYo.    LANG: EN  \n");
    printf("8b   d8 8  `8.  8    8 8    8              \n");
    printf("8`b d'8 8   `8  8YooP' 8      .oPYo. odYo. \n");
    printf("8 `o' 8 8    8  8      8   oo 8oooo8 8' `8 \n");
    printf("8     8 8   .P  8      8    8 8.     8   8 \n");
    printf("8     8 8ooo'   8      `YooP8 `Yooo' 8   8 \n");
    printf("..::::.......:::..::::::....8 :.....:..::..\n");
    printf("::::::::::::::::::::::::::::8 :::::::::::::\n");
    printf("::: by StayNoided ::::::::::..:::::::::::::\n");
    printf(":::::::::::::::::::::::::::::::::::::::::::\n\n");
    printf("This is an open-source password generator, code is available at https://gitlab.com/StayNoided/MDPGen.c\n\n\n");
    printf("\nPress [RETURN] to start the generator !\n");
    getch()0;

    while (mdpLength < 7)
    {
        printf("\n\nChoose the password's length please. (minimum 8 characters): ");
        scanf("%d",&mdpLength);
    }
    printf("\n\nYour password is: ");
    srand(time(NULL));
    do
    {

        x = genChiffre(1,6);
        sleep(10*x);
        x = rand() % 75;
        charAff(x);
        nbChar++;
    }while (nbChar < mdpLength);

    while(i <= mdpLength)
    {
        pu *= 74;
        i++;
    }
    if (pu<=45)
    {
    printf("\n\nIt would take around %d tries to crack the password.", pu);
    }
    else
    {
    printf("\n\nIt would take forever to crack the password.");
    }




}
